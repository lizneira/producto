// 15) Agregar en la siguiente clase un método que calcule y retorne el costo total de una orden.
//Además agregue un constructor que crea conveniente.
package producto;

public class Orden {

    private static  String nombreProducto;
    private static int cantUnidades;
    private static Producto[] productos;// creamos el arreglo
    private static int cantidad;
    private static double costoUnitario;
/*/
    public Orden(int maximaCantidaDeProductos) {
       this.productos = new Producto[maximaCantidaDeProductos];
       this.cantidad=0;
    }*/

    public static String getNombreProducto() {
        return nombreProducto;
    }

    public static void setNombreProducto(String nombreProducto) {
        Orden.nombreProducto = nombreProducto;
    }

    public static int getCantUnidades() {
        return cantUnidades;
    }

    public static void setCantUnidades(int cantUnidades) {
        Orden.cantUnidades = cantUnidades;
    }

    public static Producto[] getProductos() {
        return productos;
    }

    public static void setProductos(Producto productos,int pos) { //posicion
        Orden.productos[pos] = productos;
         
    }

    public static int getCantidad() {
        return cantidad;
    }

    public static void setCantidad(int cantidad) {
        Orden.cantidad = cantidad;
    }

    public static double getCostoUnitario() {
        return costoUnitario;
    }

    public static void setCostoUnitario(double costoUnitario) {
        Orden.costoUnitario = costoUnitario;
    }

  
    public static float  calcular() {
        float cant = (int) (Orden.cantUnidades * Orden.costoUnitario);
        return (float ) cant;
    }
   /*public static void agregar(Producto producto) {
        Orden.productos[Orden.cantidad] =producto;
        Orden.cantidad++;
    }
*/
    

}
